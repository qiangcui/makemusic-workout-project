
from flask import Flask, request, jsonify

__author__ = "Paul Cui"


def create_flask_app():
    app = Flask(__name__)

    from app import routes
    app.register_blueprint(routes.api)

    return app


application = create_flask_app()


if __name__ == "__main__":
    application.run(host="0.0.0.0", debug=True, port=1234)
