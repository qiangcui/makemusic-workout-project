import {Map, GoogleApiWrapper, Polyline, Marker} from 'google-maps-react';
import React, {Component} from 'react';

const style = {
    width: '60%',
    height: '60%',
    position: 'relative'
};

export class MapContainer extends Component {
    render() {
        let best_perform_path = [];
        let overall_path = [];

        // Check if there is data in the state.
        if (this.props.best_perform_map_data.length > 0) {
            best_perform_path = this.props.best_perform_map_data;
        }

        if (this.props.overall_map_data.length > 0) {
            overall_path = this.props.overall_map_data;
        }

        return(
            <Map google={this.props.google}
                 style={style}
                 className={'map'}
                 zoom={13}
                 initialCenter={{
                     lat: 40.04300,
                     lng: -105.17740
                 }}
            >

                <Polyline
                    path={best_perform_path}
                    strokeColor="red"
                    strokeOpacity={0.8}
                    strokeWeight={2}
                />
                <Polyline
                    path={overall_path}
                    strokeColor="yellow"
                    strokeOpacity={0.8}
                    strokeWeight={6}
                />
                <Marker
                    title={'Start'}
                    name={'Start'}
                    position={overall_path[0]}
                    label={{
                        text: "Departure",
                        fontFamily: "Arial",
                        fontSize: "14px",
                    }}
                />
                <Marker
                    title={'End'}
                    name={'End'}
                    position={overall_path[overall_path.length - 1]}
                    label={{
                        text: "Destination",
                        fontFamily: "Arial",
                        fontSize: "14px",
                    }}
                />
            </Map>
        )
    }
}

export default GoogleApiWrapper({
    apiKey: ("AIzaSyDqkbbseeSLCEUMTCHhdILwB0hZsZHD0OI")
})(MapContainer)
