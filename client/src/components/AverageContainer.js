import React from 'react'
import { Card } from 'semantic-ui-react'

const AverageContainer = (props) => (
    <Card>
        <Card.Content>
            <Card.Header>Best Running Average of Power</Card.Header>
            {props.best_perform_average && <Card.Description>
                {props.best_perform_average}
            </Card.Description>}
        </Card.Content>
    </Card>
);

export default AverageContainer