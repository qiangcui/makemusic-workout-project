import React, {Component} from 'react';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip} from 'recharts';


class GraphContainer extends Component {
    render() {
        return (
            <LineChart width={900} height={200} data={this.props.best_perform_graph_data}>
                <XAxis dataKey="time" label={{ value: "Time Range(s)", position: 'insideBottom', offset: -3 }}/>
                <YAxis label={{ value: "Power", angle: -90, position: 'insideLeft' }}/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Tooltip/>
                <Line type="monotone" dataKey="power" stroke="red" dot={false}/>
            </LineChart>
        );
    }
}

export default GraphContainer;
