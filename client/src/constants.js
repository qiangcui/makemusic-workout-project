import axios from 'axios';

export const UPDATE_BEST_PERFORM_DATA = "updateBestPerformData";
export const UPDATE_OVERALL_MAP_DATA= "getOverallMapDataAction";
export const API_SERVER = axios.create();