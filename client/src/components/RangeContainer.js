import React, {Component} from 'react';
import { Button } from 'semantic-ui-react'

class RangeContainer extends Component {
    updateRange = e => {
        this.props.handleRange(e.target.value);
    };

    render() {
        return (
            <div>
                <Button.Group basic>
                    <Button value="1" onClick={(e) => this.updateRange(e)}>1 Min</Button>
                    <Button value="5" onClick={(e) => this.updateRange(e)}>5 Min</Button>
                    <Button value="10" onClick={(e) => this.updateRange(e)}>10 Min</Button>
                    <Button value="15" onClick={(e) => this.updateRange(e)}>15 Min</Button>
                    <Button value="20" onClick={(e) => this.updateRange(e)}>20 Min</Button>
                </Button.Group>
            </div>
        );
    }
}

export default RangeContainer;
