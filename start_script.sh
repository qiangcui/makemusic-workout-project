#!/usr/bin/env bash

cd server
echo "Running app!"
uwsgi --http :1234 --wsgi-file wsgi.py --processes 1 --threads 1 &

cd ../client
npm install
npm start &

