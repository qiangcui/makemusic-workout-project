import { combineReducers } from "redux";
import WorkoutDataReducer from './reducers/workoutDataReducer';

export default combineReducers({
   workout_data: WorkoutDataReducer
});