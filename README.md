Note: This project was developed by Qiang (Paul) Cui.
## Important

In the graph, there are a list of numbers in the x axis of graph. They are the passed time in seconds from the start point.
For example, 1029 means 1029 seconds after starting moving.

## How To Run and Use Application?

Without Docker (Recommended)

1. Please pull the repository by typing ```git clone https://gitlab.com/qiangcui/makemusic-workout-project.git``` in the terminal.

2. Make sure the port 3000 and the port 1234 are available.

3. Run ```./start_script.sh``` at the project default directory.

4. Open browser and hit ```localhost:3000```.

5. Check if there is a yellow line in the google map or not.

6. Click any minutes in the range section.

7. Check if there are red lines in the graph and map.
 

With Docker

1. Please pull the repository by typing ```git clone https://gitlab.com/qiangcui/makemusic-workout-project.git``` in the terminal.

2. Make sure you installed docker and docker-compose in your work station.

3. Please open the terminal and type ```docker login``` and put your docker hub username and password correctly and log in successfully.

4. If your work station is PC, please type ```ipconfig``` in cmd terminal and find out the ip address of your work station.

5. If your work station is Mac/Linux, please types ```ifconfig``` in terminal and find out the ip address of your work station.

6. Go to client/package.json file and open it.

7. Change the old ip address in the proxy section with your ip address - ```"proxy": "http://{Put your ip address here}:1234".```

8. Open the terminal and jump to root directory where the docker-compose file is located at.

9. Run ```docker-compose up --build``` in terminal.

10. In Chrome Browser, hit ```localhost:3000```.

11. Check if there is a yellow line in the google map or not.

12. Click any minutes in the range section.

13. Check if there are red lines in the graph and map.
