import { API_SERVER } from './constants';

export const getBestPerformDataAPI = (range_min) => (
    API_SERVER.get('api/workout?range_min=' + range_min).then(res => res).catch(error => error.response)
);

export const getOverallMapDataAPI = () => (
    API_SERVER.get('api/total').then(res => res).catch(error => error.response)
);
