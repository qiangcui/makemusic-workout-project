import {
    UPDATE_BEST_PERFORM_DATA,
    UPDATE_OVERALL_MAP_DATA
} from "../../constants";
import { getBestPerformDataAPI, getOverallMapDataAPI } from "../../api";

export function getOverallMapData() {
    return dispatch => {
        getOverallMapDataAPI()
            .then(
                res => {
                    dispatch(getOverallMapDataAction(res['data']));
                }
            )
            .catch(
                error => error
            )
    }
}

export const getOverallMapDataAction = (overall_data) => {
    return {
        type: UPDATE_OVERALL_MAP_DATA,
        payload: {
            data: overall_data
        }
    }
};

export function getBestPerformData(range_min) {
    return dispatch => {
        getBestPerformDataAPI(range_min)
            .then(
                res => {
                    dispatch(getBestPerformDataAction(res['data']));
                }
            )
            .catch(
                error => error
            )
    }
}

export const getBestPerformDataAction = (new_workout_data) => {
    return {
        type: UPDATE_BEST_PERFORM_DATA,
        payload: {
            data: new_workout_data
        }
    }
};
