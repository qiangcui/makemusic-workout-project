from flask import request, jsonify, Blueprint
from process_file import DAL
import os

GET = 'GET'
POST = 'POST'
PUT = 'PUT'
DELETE = 'DELETE'
DAL = DAL()

api = Blueprint('api', __name__)

static_file_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'static')


@api.route('/', methods=[GET])
def index():
    return "Welcome to project-workout-power-server!!!"


@api.route('/api/total', methods=[GET])
def get_overall_map_data():
    rst = DAL.get_overall_map_data()
    return jsonify(rst)


@api.route('/api/workout', methods=[GET])
def get_best_perform_data():
    rst = DAL.get_best_perform_data(request.args.get('range_min'))
    return jsonify(rst)



