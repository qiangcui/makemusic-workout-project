import React, { Component } from 'react';
import { connect } from "react-redux";
import { getBestPerformData, getOverallMapData } from "./redux/actions/workoutDataAction";
import { Container, Grid } from 'semantic-ui-react';
import GraphContainer from './components/GraphContainer';
import MapContainer from './components/MapContainer';
import RangeContainer from './components/RangeContainer';
import AverageContainer from "./components/AverageContainer";


class App extends Component {

    componentDidMount () {
        this.props.getOverallMapData();
    };

    handleRange = (range_min) => {
        this.props.getBestPerformData(range_min);
    };

    render() {
        return (
            <div>
                <Container style={{ marginTop: '50px' }}>
                    <Grid>
                        <Grid.Column floated="left" width={13}>
                            <GraphContainer best_perform_graph_data={this.props.best_perform_graph_data} best_perform_average={this.props.best_perform_average}/>
                            <RangeContainer handleRange={this.handleRange}/>
                        </Grid.Column>
                        <Grid.Column floated="right" width={3}>
                            <AverageContainer best_perform_average={this.props.best_perform_average} />
                        </Grid.Column>
                    </Grid>
                </Container>
                <Container style={{ marginTop: '30px' }}>
                    <div>
                        <MapContainer overall_map_data={this.props.overall_map_data} best_perform_map_data={this.props.best_perform_map_data}/>
                    </div>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    best_perform_overall_data: state.workout_data.best_perform_overall_data,
    best_perform_graph_data: state.workout_data.best_perform_graph_data,
    best_perform_map_data: state.workout_data.best_perform_map_data,
    best_perform_average: state.workout_data.best_perform_average,
    overall_map_data: state.workout_data.overall_map_data,
});

const mapActionsToProps = {
    getBestPerformData: getBestPerformData,
    getOverallMapData: getOverallMapData
};

export default connect(
    mapStateToProps, mapActionsToProps
)(App);
