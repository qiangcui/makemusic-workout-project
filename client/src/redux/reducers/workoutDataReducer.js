import {
    UPDATE_BEST_PERFORM_DATA,
    UPDATE_OVERALL_MAP_DATA
} from "../../constants";

const initialState = {
    best_perform_overall_data: [],
    best_perform_graph_data: [],
    best_perform_map_data: [],
    best_perform_average: 0,
    overall_map_data: []
};

const WorkoutDataReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_BEST_PERFORM_DATA:
            return {
                ...state,
                best_perform_overall_data: [...action.payload.data['best_perform_data']],
                best_perform_graph_data: [...action.payload.data['best_perform_graph_data']],
                best_perform_map_data: [...action.payload.data['best_perform_map_data']],
                best_perform_average: action.payload.data['best_perform_average']
            };
        case UPDATE_OVERALL_MAP_DATA:
            return {
                ...state,
                overall_map_data: [...action.payload.data['overall_map_data']]
            };
        default:
            return state;
    }
};

export default WorkoutDataReducer;