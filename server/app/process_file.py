from collections import deque
import json
import os


class DAL(object):
    def __init__(self):
        self.queue = deque([])
        self.sum = 0.0
        self.range_size = 0
        self.max_average = 0
        self.max_average_index = 0

    def fetch_data_from_file(self):
        # Get workout-data.json file's absolute path
        data_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'static/data/workout-data.json')

        with open(data_dir, 'r') as json_file:
            data = json.load(json_file)

        return data

    def get_overall_map_data(self):
        data = self.fetch_data_from_file()

        # Extract the map data from the overall data and send back
        overall_map_data = []
        for i in data['samples']:
            if 'positionLat' in i['values']:
                overall_map_data.append(
                    {
                        'lat': float(i['values']['positionLat']),
                        'lng': float(i['values']['positionLong'])
                    }
                )

        return {'overall_map_data': overall_map_data}

    def get_best_perform_data(self, range_size):
        # Reset static values
        self.queue = deque([])
        self.sum = 0.0
        self.range_size = 0
        self.max_average = 0
        self.max_average_index = 0

        # Fetch overall data from the file
        data = self.fetch_data_from_file()

        # Extract the useful information such as power, time, location from the data
        useful_data = []
        for i in data['samples']:
            tmp = {}
            if 'positionLat' in i['values']:
                if 'power' not in i['values']:
                    tmp['power'] = 0
                else:
                    tmp['power'] = i['values']['power']

                tmp['time'] = i['millisecondOffset']/1000
                tmp['positionLat'] = i['values']['positionLat']
                tmp['positionLong'] = i['values']['positionLong']
                useful_data.append(tmp)

        # Process data
        result = self.find_max_average(useful_data, int(range_size) * 60)
        return result

    def find_max_average(self, useful_data, range_size):
        self.range_size = range_size

        # Find the maximum running average with next function.
        for index in range(len(useful_data)):
            average = self.next(useful_data[index]['power'])
            if self.max_average < average:
                self.max_average = average
                self.max_average_index = index

        best_perform_data = useful_data[self.max_average_index - int(self.range_size) + 1: self.max_average_index + 1]

        # Split the best perform data into best perform graph data and best perform map data.
        best_perform_graph_data = []
        best_perform_map_data = []

        for row in best_perform_data:
            tmp1 = {key: value for i, (key, value) in enumerate(row.viewitems()) if key == 'time' or key == 'power'}
            tmp2 = {}

            for i, (key, value) in enumerate(row.viewitems()):
                if key == 'positionLat':
                    tmp2['lat'] = float(value)
                if key == 'positionLong':
                    tmp2['lng'] = float(value)

            best_perform_graph_data.append(tmp1)
            best_perform_map_data.append(tmp2)

        return {'best_perform_average': self.max_average,
                'best_perform_graph_data': best_perform_graph_data,
                'best_perform_map_data': best_perform_map_data,
                'best_perform_data': best_perform_data}

    def next(self, val):
        if len(self.queue) == self.range_size:
            self.sum -= self.queue.popleft()

        self.sum += val
        self.queue.append(val)
        return self.sum / len(self.queue)
